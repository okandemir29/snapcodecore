﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.Model.Dto;
    using Snapcode.WebUI.Site.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    public class ProfileController : Controller
    {
        ProfileData _profileData;
        ReviewData _reviewData;
        Cache.ProfileDbCacheHelper profileDbCacheHelper;

        public ProfileController(ProfileData _profileData
            , ReviewData _reviewData
            , Cache.ProfileDbCacheHelper profileDbCacheHelper
            )
        {
            this._profileData = _profileData;
            this._reviewData = _reviewData;
            this.profileDbCacheHelper = profileDbCacheHelper;
        }

        public IActionResult Index(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { q = "username-empty" });

            var profile = new Model.Profile();
            var relateds = new List<Model.Dto.ProfileForListSimpleDto>();

            var cacheProfile = profileDbCacheHelper.GetProfileByUsername(username);
            if(cacheProfile != null)
            {
                profile = cacheProfile.Profile;
                relateds = cacheProfile.RelatedProfiles.OrderBy(x => Guid.NewGuid()).Take(9).ToList();
            }
                
            if(profile == null)
            {
                profile = _profileData.GetBy(x => x.Username == username).FirstOrDefault();
                if (profile == null)
                    return RedirectToAction("Index", "Home", new { q = "profile-not-found" });

                var relateds_model = _profileData.GetBy(x => x.CategoryId == profile.CategoryId).OrderBy(x => Guid.NewGuid()).Take(9).ToList();

                var related_list = new List<ProfileForListSimpleDto>();
                foreach (var item in relateds_model)
                {
                    related_list.Add(new ProfileForListSimpleDto()
                    {
                        BoorCode = item.BoorCode,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        OverlayPath = item.OverlayPath,
                        ProfileId = item.Id,
                        Username = item.Username,
                        About = item.About,
                        CategoryId = item.CategoryId,
                        Interests = item.Interests,
                    });
                }
                profileDbCacheHelper.Insert(profile, related_list);
                relateds = related_list;
            }

            var reviews = _reviewData.GetBy(x => x.UserId == profile.Id && !x.IsPopular && x.IsActive);

            var model = new ProfileViewModel()
            {
                Profile = profile,
                Relateds = relateds,
                Reviews = reviews,
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(int Point, string Review, string Fullname, string Email, int ProfileId)
        {
            var errors = new List<string>();
            if (Point <= 0) errors.Add("Please select any point");
            if(string.IsNullOrEmpty(Review)) errors.Add("Comment input required");
            if(string.IsNullOrEmpty(Fullname)) errors.Add("Fullname input required");
            if(string.IsNullOrEmpty(Email)) errors.Add("Email input required");

            var profile = _profileData.GetByKey(ProfileId);
            if (profile == null)
                return RedirectToAction("Index", "Home", new { q = "not_found_profile" });

            if (errors.Count > 0)
            {
                ViewBag.Error = errors;
                return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "inputs required" });
            }
            
            var review = new Model.Review()
            {
                CreateDate = DateTime.Now,
                Description = Review,
                Fullname = Fullname,
                IsActive = true,
                IsPopular = false,
                IsRead = false,
                Star = Point,
                UserId = ProfileId,
                Email = Email,
            };

            var insert_review = _reviewData.Insert(review);
            if(insert_review.IsSucceed)
                return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "review-sended" });

            errors.Add("Server Error Please Later Try Again");
            return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "server-error" });
        }

        [HttpGet]
        public IActionResult Add()
        {
            var model = new Model.Profile();
            return View(model);
        }

        [HttpPost]
        public IActionResult Add(Model.Profile model)
        {
            var errorList = new List<string>();
            if (string.IsNullOrEmpty(model.Username))
                errorList.Add("Username is empty*");
            if (string.IsNullOrEmpty(model.Email))
                errorList.Add("Mail is empty*");
            if (string.IsNullOrEmpty(model.FirstName))
                errorList.Add("FirstName is empty*");
            if (string.IsNullOrEmpty(model.LastName))
                errorList.Add("LastName is empty*");
            if (model.CategoryId <= 0)
                errorList.Add("Select your category*");

            if (errorList.Count > 0)
            {
                ViewBag.Error = errorList;
                return View(model);
            }

            var profile = new Model.Profile()
            {
                BirthDate = DateTime.Now,
                BoorCode = "",
                CategoryId = model.CategoryId,
                Facebook = model.Facebook ?? "",
                FirstName = model.FirstName,
                Instagram = model.Instagram ?? "",
                Interests = model.Interests ?? "",
                LastName = model.LastName,
                OverlayPath = "",
                Pinterest = model.Pinterest ?? "",
                About = model.About ?? "",
                TotalLike = 0,
                Twitter = model.Twitter ?? "",
                Youtube = model.Youtube ?? "",
                Email = model.Email,
                Username = model.Username,
                Description = "",
                IsActive = false,
                IsNsfw = false,
            };

            using (var client = new WebClient())
            {
                try
                {
                    var path = $"https://feelinsonice-hrd.appspot.com/web/deeplink/snapcode?username={profile.Username}&type=PNG";
                    var codePath = $"wwwroot/images/{profile.Username}.png";
                    client.DownloadFile(path, codePath);
                    profile.BoorCode = codePath.Replace("wwwroot","");
                    profile.OverlayPath = codePath.Replace("wwwroot", "");
                }
                catch (Exception exc)
                {
                    ViewBag.Error = string.Join("<br />", "Can't get Snapchat Code of this user! Sorry :(");
                    return View(model);
                }
            }

            var insert = _profileData.Insert(profile);
            if (insert.IsSucceed)
            {
                model = new Model.Profile()
                {
                    Youtube = "",
                    Twitter = "",
                    Pinterest = "",
                    LastName = "",
                    About = "",
                    CategoryId = -1,
                    Facebook = "",
                    FirstName = "",
                    Instagram = "",
                    Interests = "",
                    Username = "",
                    Email = "",
                };

                ViewBag.Success = "Your request is saved, soon add your profile on this site";
                return View(model);
            }
            else
            {
                errorList.Add("Upss... Server error later try again...");
                ViewBag.Error = errorList;
                return View(model);
            }
        }

        public async Task<string> ProfileLists(int CategoryId, int ProfileId)
        {
            var count = profileDbCacheHelper.GetRelatedProfilesCount(CategoryId);
            if (count > 250)
                return "";

            var relateds_model = _profileData.GetBy(x => x.CategoryId == CategoryId).OrderBy(x => Guid.NewGuid()).Take(9).ToList();

            var related_list = new List<ProfileForListSimpleDto>();
            foreach (var item in relateds_model)
            {
                related_list.Add(new ProfileForListSimpleDto()
                {
                    BoorCode = item.BoorCode,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    OverlayPath = item.OverlayPath,
                    ProfileId = item.Id,
                    Username = item.Username,
                    About = item.About,
                    CategoryId = item.CategoryId,
                    Interests = item.Interests,
                });
            }
            profileDbCacheHelper.InsertRelated(related_list, ProfileId);

            return "";
        }
    }
}
