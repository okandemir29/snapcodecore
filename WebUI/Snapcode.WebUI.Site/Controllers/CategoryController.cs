﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.WebUI.Site.Models;
    using System.Linq;

    public class CategoryController : Controller
    {
        ProfileData _profileData;
        CategoryData _categoryData;
        Infrastructure.Cache.CacheHelper _cacheHelper;

        public CategoryController(ProfileData _profileData
            , CategoryData _categoryData
            , Infrastructure.Cache.CacheHelper _cacheHelper
            )
        {
            this._profileData = _profileData;
            this._categoryData = _categoryData;
            this._cacheHelper = _cacheHelper;
        }

        public IActionResult Index(string slug, int page=1)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "slug-empty" });

            var category = _cacheHelper.Categories.Where(x => x.Slug == slug).FirstOrDefault();
            if (category == null)
                return RedirectToAction("Index", "Home", new { q = "category-not-found" });

            var model = new CategoryViewModel()
            {
                Category = category,
            };

            var profiles = _profileData.GetByPage(x => x.CategoryId == category.Id, page, 15);
            var totalItem = _profileData.GetCount(x => x.CategoryId == category.Id);

            model.Profiles = profiles;
            model.TotalCount = totalItem;
            model.TotalPage = totalItem / 15;
            model.TotalPage++;
            model.Page = page;

            return View(model);
        }
    }
}
