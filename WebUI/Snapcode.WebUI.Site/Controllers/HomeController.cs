﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using System.Linq;

    public class HomeController : Controller
    {
        ProfileData profileData;
        Infrastructure.Cache.CacheHelper _cacheHelper;

        public HomeController(ProfileData profileData, Infrastructure.Cache.CacheHelper _cacheHelper)
        {
            this.profileData = profileData;
            this._cacheHelper = _cacheHelper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult ClearSetting()
        {
            _cacheHelper.SettingClear();
            return Json(new { result = true });
        }

        public JsonResult ClearCategory()
        {
            _cacheHelper.CategoriesClear();
            return Json(new { result = true });
        }

        public JsonResult ClearProfile()
        {
            _cacheHelper.ProfilesClear();
            return Json(new { result = true });
        }

        public JsonResult ClearList()
        {
            _cacheHelper.ListsClear();
            return Json(new { result = true });
        }

        public JsonResult ClearPopular()
        {
            _cacheHelper.PopularAccountClear();
            return Json(new { result = true });
        }

        public JsonResult ClearAds()
        {
            _cacheHelper.AdsAccountClear();
            return Json(new { result = true });
        }
    }
}
