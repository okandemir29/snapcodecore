﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.WebUI.Site.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PopularController : Controller
    {
        PopularData _popularData;
        ReviewData _reviewData;

        public PopularController(PopularData _popularData
            , ReviewData _reviewData
            )
        {
            this._popularData = _popularData;
            this._reviewData = _reviewData;
        }

        public IActionResult Index(string username)
        {
            if (string.IsNullOrEmpty(username))
                return RedirectToAction("Index", "Home", new { q = "username-empty" });

            var profile = _popularData.GetBy(x => x.Username == username).FirstOrDefault();
            if (profile == null)
                return RedirectToAction("Index", "Home", new { q = "profile-not-found" });

            var reviews = _reviewData.GetBy(x => x.UserId == profile.Id && x.IsPopular && x.IsActive);

            var model = new PopularViewModel()
            {
                Profile = profile,
                Relateds = _popularData.GetBy(x=>x.ListId == profile.ListId).OrderBy(x=> Guid.NewGuid()).Take(9).ToList(),
                Reviews = reviews,
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(int Point, string Review, string Fullname, string Email, int ProfileId)
        {
            var errors = new List<string>();
            if (Point <= 0) errors.Add("Please select any point");
            if (string.IsNullOrEmpty(Review)) errors.Add("Comment input required");
            if (string.IsNullOrEmpty(Fullname)) errors.Add("Fullname input required");
            if (string.IsNullOrEmpty(Email)) errors.Add("Email input required");

            var profile = _popularData.GetByKey(ProfileId);
            if (profile == null)
                return RedirectToAction("Index", "Home", new { q = "not_found_profile" });

            if (errors.Count > 0)
            {
                ViewBag.Error = errors;
                return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "inputs required" });
            }

            var review = new Model.Review()
            {
                CreateDate = DateTime.Now,
                Description = Review,
                Fullname = Fullname,
                IsActive = true,
                IsPopular = false,
                IsRead = false,
                Star = Point,
                UserId = ProfileId,
                Email = Email,
            };

            var insert_review = _reviewData.Insert(review);
            if (insert_review.IsSucceed)
                return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "review-sended" });

            errors.Add("Server Error Please Later Try Again");
            return RedirectToAction("Index", "Profile", new { username = profile.Username, q = "server-error" });
        }
    }
}
