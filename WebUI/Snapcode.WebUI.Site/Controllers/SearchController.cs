﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.WebUI.Site.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SearchController : Controller
    {
        ProfileData _profileData;
        HashtagData _hashtagData;

        public SearchController(ProfileData _profileData
            , HashtagData _hashtagData
            )
        {
            this._profileData = _profileData;
            this._hashtagData = _hashtagData;
        }

        public IActionResult Index(string query)
        {
            if (string.IsNullOrEmpty(query))
                return RedirectToAction("Index", "Home", new { q = "query-empty" });

            var profiles = _profileData.GetBy(x => x.Username.Contains(query));
            var hashtags = _hashtagData.GetBy(x => x.Name.Contains(query));

            var model = new SearchViewModel()
            {
                Hashtags = hashtags,
                Profiles = profiles,
                Query = query,
            };

            return View(model);
        }
    }
}
