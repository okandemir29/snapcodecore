﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.WebUI.Site.Models;
    using System.Linq;

    public class ListController : Controller
    {
        PopularData _popularData;
        Infrastructure.Cache.CacheHelper _cacheHelper;

        public ListController(PopularData _popularData
            , Infrastructure.Cache.CacheHelper _cacheHelper
            )
        {
            this._popularData = _popularData;
            this._cacheHelper = _cacheHelper;
        }

        public IActionResult Index(string slug, int page=1)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "slug-empty" });

            var list = _cacheHelper.Lists.Where(x => x.Slug == slug).FirstOrDefault();
            if (list == null)
                return RedirectToAction("Index", "Home", new { q = "list-not-found" });

            var model = new ListViewModel()
            {
                List = list,
            };

            var profiles = _popularData.GetByPage(x => x.ListId == list.Id, page, 15);
            var totalItem = _popularData.GetCount(x => x.ListId == list.Id);

            model.Populars = profiles;
            model.TotalCount = totalItem;
            model.TotalPage = totalItem / 15;
            model.TotalPage++;
            model.Page = page;

            return View(model);
        }
    }
}
