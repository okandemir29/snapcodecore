﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using System.Collections.Generic;
    using System.Linq;

    public class RemoveController : Controller
    {
        RemovalData _removalData;

        public RemoveController(RemovalData _removalData)
        {
            this._removalData = _removalData;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = new Model.Removal();
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(string username, string email, string message, string link)
        {
            var errors = new List<string>();

            if (string.IsNullOrEmpty(username)) errors.Add("username is required");
            if (string.IsNullOrEmpty(email)) errors.Add("email is required");
            if (string.IsNullOrEmpty(message)) errors.Add("message is required");
            if (string.IsNullOrEmpty(link)) errors.Add("link is required");

            var remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;

            var model = new Model.Removal()
            {
                Description = message,
                IsDeleted = false,
                IsDmca = false,
                IsRead = false,
                Mail = email,
                Username = username,
                Url = link,
                IpAddress = remoteIpAddress.ToString(),
            };

            if (errors.Count > 0)
            {
                ViewBag.IsSucceed = false;
                ViewBag.Message = errors[0];
                return View(model);
            }

            var insert = _removalData.Insert(model);
            if (insert.IsSucceed)
            {
                ViewBag.IsSucceed = true;
                ViewBag.Message = "Will be able to remove it as soon as possible";
                return View(model);
            }

            ViewBag.IsSucceed = false;
            ViewBag.Message = insert.Message;

            return View(model);
        }
    }
}
