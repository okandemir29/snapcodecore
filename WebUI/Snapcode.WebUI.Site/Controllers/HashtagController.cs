﻿namespace Snapcode.WebUI.Site.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Snapcode.Data;
    using Snapcode.WebUI.Site.Models;
    using System;
    using System.Linq;

    public class HashtagController : Controller
    {
        ProfileData _profileData;
        HashtagData _hashtagData;
        Infrastructure.Cache.CacheHelper _cacheHelper;

        public HashtagController(ProfileData _profileData
            , HashtagData _hashtagData
            , Infrastructure.Cache.CacheHelper _cacheHelper
            )
        {
            this._profileData = _profileData;
            this._hashtagData = _hashtagData;
            this._cacheHelper = _cacheHelper;
        }

        public IActionResult Index(string slug, int page=1)
        {
            var name = slug;
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { q = "hashtag-empty" });

            var hashtag = _hashtagData.GetBy(x => x.Name == slug).FirstOrDefault();
            if (hashtag == null)
            {
                if (slug.Contains(" "))
                    name = slug.Split(' ')[0];
                else
                    name = slug;

                var hModel = new Model.Hashtag()
                {
                    isBan = false,
                    Name = name,
                    IsDmca = false,
                };

                var insert = _hashtagData.Insert(hModel);
                if (!insert.IsSucceed)
                    return RedirectToAction("Index", "Home", new { q = "hashtag-not-found" });

                hashtag = hModel;
            }

            if (hashtag.IsDmca)
                name = "funny";

            var model = new HashtagViewModel()
            {
                Hashtag = hashtag,
            };

            var profiles = _profileData.GetByPage(x => x.Interests.Contains(name), page, 15);
            var totalItem = _profileData.GetCount(x => x.Interests.Contains(name));

            if (profiles.Count <= 0)
            {
                profiles = _cacheHelper.Profiles.OrderBy(x=> Guid.NewGuid()).Take(18).ToList();
            }

            model.Tag = name;
            model.Profiles = profiles;
            model.TotalCount = totalItem;
            model.TotalPage = totalItem / 15;
            model.TotalPage++;
            model.Page = page;

            return View(model);
        }
    }
}
