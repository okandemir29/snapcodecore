﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Snapcode.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Snapcode.WebUI.Site.Cache
{
    public class ProfileDbCacheHelper
    {
        readonly string dbFilePath;

        public ProfileDbCacheHelper(IHostingEnvironment hostEnvironment)
        {
            var dbFilePathDir = $"{hostEnvironment.ContentRootPath}/_caches";
            if (!System.IO.Directory.Exists(dbFilePathDir))
                System.IO.Directory.CreateDirectory(dbFilePathDir);

            dbFilePath = $"{dbFilePathDir}/cache.db";
        }

        public void Insert(Model.Profile profile, List<ProfileForListSimpleDto> relatedProfiles)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                var commandContent = connection.CreateCommand();
                commandContent.CommandText = "INSERT INTO profiles (About, BoorCode, CategoryId, BirthDate, Email, FirstName, LastName, OverlayPath, Pinterest,Username,TotalLike,Youtube,Facebook,Twitter, Instagram,Interests,IsNsfw,IsActive,Description,ProfileId) " +
                    "VALUES ($about,$boorCode, $categoryId, $birthDate, $email, $firstName, $lastName, $overlayPath, $pinterest,$username,$totalLike,$youtube,$facebook,$twitter,$instagram,$interests,$isNsfw,$isActive,$description,$profileId)";

                commandContent.Parameters.AddWithValue("$about", profile.About);
                commandContent.Parameters.AddWithValue("$boorCode", profile.BoorCode);
                commandContent.Parameters.AddWithValue("$categoryId", profile.CategoryId);
                commandContent.Parameters.AddWithValue("$birthDate", profile.BirthDate);
                commandContent.Parameters.AddWithValue("$email", profile.Email);
                commandContent.Parameters.AddWithValue("$firstName", profile.FirstName);
                commandContent.Parameters.AddWithValue("$lastName", profile.LastName);
                commandContent.Parameters.AddWithValue("$overlayPath", profile.OverlayPath);
                commandContent.Parameters.AddWithValue("$pinterest", profile.Pinterest);
                commandContent.Parameters.AddWithValue("$username", profile.Username);
                commandContent.Parameters.AddWithValue("$totalLike", profile.TotalLike);
                commandContent.Parameters.AddWithValue("$youtube", profile.Youtube);
                commandContent.Parameters.AddWithValue("$facebook", profile.Facebook);
                commandContent.Parameters.AddWithValue("$twitter", profile.Twitter);
                commandContent.Parameters.AddWithValue("$instagram", profile.Instagram);
                commandContent.Parameters.AddWithValue("$interests", profile.Interests);
                commandContent.Parameters.AddWithValue("$isNsfw", profile.IsNsfw);
                commandContent.Parameters.AddWithValue("$isActive", profile.IsActive);
                commandContent.Parameters.AddWithValue("$description", profile.Description ?? "");
                commandContent.Parameters.AddWithValue("$profileId", profile.Id);

                var relatedProfileCommands = new List<SqliteCommand>();
                if (relatedProfiles.Count > 0)
                {
                    var commandTextRelated = "INSERT INTO relateds (ProfileId, FirstName, LastName, Username, BoorCode, OverlayPath, Interests, About, CategoryId) " +
                        "VALUES ($profileId,$firstName,$lastName,$username,$boorCode,$overlayPath,$interests,$about,$categoryId)";

                    foreach (var relatedProfile in relatedProfiles)
                    {
                        var commandRelated = connection.CreateCommand();
                        commandRelated.CommandText = commandTextRelated;

                        commandRelated.Parameters.AddWithValue("$profileId", profile.Id);
                        commandRelated.Parameters.AddWithValue("$firstName", relatedProfile.FirstName);
                        commandRelated.Parameters.AddWithValue("$lastName", relatedProfile.LastName);
                        commandRelated.Parameters.AddWithValue("$username", relatedProfile.Username);
                        commandRelated.Parameters.AddWithValue("$boorCode", relatedProfile.BoorCode);
                        commandRelated.Parameters.AddWithValue("$overlayPath", relatedProfile.OverlayPath);
                        commandRelated.Parameters.AddWithValue("$interests", relatedProfile.Interests);
                        commandRelated.Parameters.AddWithValue("$about", relatedProfile.About);
                        commandRelated.Parameters.AddWithValue("$categoryId", relatedProfile.CategoryId);

                        relatedProfileCommands.Add(commandRelated);
                    }
                }

                try
                {
                    commandContent.ExecuteNonQuery();
                    relatedProfileCommands.ForEach(x => x.ExecuteNonQuery());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertRelated(List<ProfileForListSimpleDto> relatedProfiles, int profile_id)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                var relatedProfileCommands = new List<SqliteCommand>();
                if (relatedProfiles.Count > 0)
                {
                    var commandTextRelated = "INSERT INTO relateds (ProfileId, FirstName, LastName, Username, BoorCode, OverlayPath, Interests, About, CategoryId) " +
                        "VALUES ($profileId,$firstName,$lastName,$username,$boorCode,$overlayPath,$interests,$about,$categoryId)";

                    foreach (var relatedProfile in relatedProfiles)
                    {
                        var commandRelated = connection.CreateCommand();
                        commandRelated.CommandText = commandTextRelated;

                        commandRelated.Parameters.AddWithValue("$profileId", profile_id);
                        commandRelated.Parameters.AddWithValue("$firstName", relatedProfile.FirstName);
                        commandRelated.Parameters.AddWithValue("$lastName", relatedProfile.LastName);
                        commandRelated.Parameters.AddWithValue("$username", relatedProfile.Username);
                        commandRelated.Parameters.AddWithValue("$boorCode", relatedProfile.BoorCode);
                        commandRelated.Parameters.AddWithValue("$overlayPath", relatedProfile.OverlayPath);
                        commandRelated.Parameters.AddWithValue("$interests", relatedProfile.Interests);
                        commandRelated.Parameters.AddWithValue("$about", relatedProfile.About);
                        commandRelated.Parameters.AddWithValue("$categoryId", relatedProfile.CategoryId);

                        relatedProfileCommands.Add(commandRelated);
                    }
                }

                try
                {
                    relatedProfileCommands.ForEach(x => x.ExecuteNonQuery());
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool DeleteBySlug(string username)
        {
            var profileInDb = GetProfileByUsername(username);
            if (profileInDb.Profile == null)
            {
                return true;
            }

            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContents = connection.CreateCommand();
                commandContents.CommandText = $"DELETE FROM contents WHERE username=$username;";
                commandContents.Parameters.AddWithValue("username", username);

                var commandRelateds = connection.CreateCommand();
                commandRelateds.CommandText = $"DELETE FROM relateds WHERE ProfileId=$profileId;";
                commandRelateds.Parameters.AddWithValue("$profileId", profileInDb.ProfileId);

                try
                {
                    commandContents.ExecuteNonQuery();
                    commandRelateds.ExecuteNonQuery();

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public List<Model.Profile> GetByPage(int page = 1, int rowCount = 20)
        {
            if (page < 1)
                page = 1;

            var list = new List<Model.Profile>();
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContent = connection.CreateCommand();
                commandContent.CommandText = @"SELECT * FROM profiles ORDER BY Id LIMIT " + ((page - 1) * rowCount) + "," + rowCount + ";";

                using (var reader = commandContent.ExecuteReader())
                {
                    if (!reader.HasRows)
                        return list;

                    while (reader.Read())
                        list.Add(ReaderToContentMap(reader));
                }

                connection.Close();
            }

            return list;
        }

        public int GetProfilesCount()
        {
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContent = connection.CreateCommand();
                commandContent.CommandText = @"SELECT COUNT(*) FROM profiles";

                int rowCount = 0;
                rowCount = Convert.ToInt32(commandContent.ExecuteScalar());

                connection.Close();

                return rowCount;
            }
        }

        public int GetRelatedProfilesCount(int category_id)
        {
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContent = connection.CreateCommand();
                commandContent.CommandText = @"SELECT COUNT(*) FROM relateds WHERE CategoryId = $categoryId";
                commandContent.Parameters.AddWithValue("$categoryId", category_id);

                int rowCount = 0;
                rowCount = Convert.ToInt32(commandContent.ExecuteScalar());

                connection.Close();

                return rowCount;
            }
        }

        public ProfileDbCacheDto GetProfileByUsername(string username)
        {
            var contentDbCacheDto = new ProfileDbCacheDto();
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContent = connection.CreateCommand();
                commandContent.CommandText = @"SELECT * FROM profiles WHERE Username = $username";
                commandContent.Parameters.AddWithValue("$username", username);

                using (var reader = commandContent.ExecuteReader())
                {
                    if (!reader.HasRows)
                        return contentDbCacheDto;

                    while (reader.Read())
                    {
                        contentDbCacheDto.Profile = ReaderToContentMap(reader);
                        contentDbCacheDto.ProfileId = contentDbCacheDto.Profile.Id;
                        contentDbCacheDto.CategoryId = contentDbCacheDto.Profile.CategoryId;
                    }
                }

                var commandRelated = connection.CreateCommand();
                commandRelated.CommandText = @"SELECT * FROM relateds WHERE CategoryId = $categoryId";
                commandRelated.Parameters.AddWithValue("$categoryId", contentDbCacheDto.CategoryId);
                using (var reader = commandRelated.ExecuteReader())
                {
                    while (reader.Read())
                        contentDbCacheDto.RelatedProfiles.Add(ReaderToContentSimple(reader));
                }

                connection.Close();
            }

            return contentDbCacheDto;
        }

        public ProfileDbCacheDto GetProfileById(int profileId)
        {
            var contentDbCacheDto = new ProfileDbCacheDto();
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContent = connection.CreateCommand();
                commandContent.CommandText = @"SELECT * FROM profiles WHERE ProfileId = $profileId";
                commandContent.Parameters.AddWithValue("$profileId", profileId);

                using (var reader = commandContent.ExecuteReader())
                {
                    if (!reader.HasRows)
                        return contentDbCacheDto;

                    while (reader.Read())
                    {
                        contentDbCacheDto.Profile = ReaderToContentMap(reader);
                        contentDbCacheDto.ProfileId = contentDbCacheDto.Profile.Id;
                        contentDbCacheDto.CategoryId = contentDbCacheDto.Profile.CategoryId;
                    }
                }


                var commandRelated = connection.CreateCommand();
                commandRelated.CommandText = @"SELECT * FROM relateds WHERE CategoryId = $categoryId";
                commandRelated.Parameters.AddWithValue("$categoryId", contentDbCacheDto.CategoryId);
                using (var reader = commandRelated.ExecuteReader())
                {
                    while (reader.Read())
                        contentDbCacheDto.RelatedProfiles.Add(ReaderToContentSimple(reader));
                }

                connection.Close();
            }

            return contentDbCacheDto;
        }

        public void DropTables()
        {
            using (var connection = GetConnection())
            {
                connection.Open();

                var commandContents = connection.CreateCommand();
                commandContents.CommandText = $"DROP TABLE IF EXISTS profiles";

                var commandRelateds = connection.CreateCommand();
                commandRelateds.CommandText = $"DROP TABLE IF EXISTS relateds";

                try
                {
                    commandContents.ExecuteNonQuery();
                    commandRelateds.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InitTables()
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                
                var createTableCommandContents = connection.CreateCommand();
                createTableCommandContents.CommandText = @"
                        CREATE TABLE IF NOT EXISTS [profiles] (
                          [Id] INTEGER NOT NULL
                          , [About] TEXT NOT NULL
                          , [BoorCode] TEXT NOT NULL
                          , [CategoryId] INTEGER NOT NULL
                          , [BirthDate] TEXT NOT NULL
                          , [Email] TEXT NOT NULL
                          , [FirstName] TEXT NOT NULL
                          , [LastName] TEXT NOT NULL
                          , [OverlayPath] TEXT NOT NULL
                          , [Pinterest] TEXT NOT NULL
                          , [Username] TEXT NOT NULL
                          , [TotalLike] INTEGER NOT NULL
                          , [Youtube] TEXT NOT NULL
                          , [Facebook] TEXT NOT NULL
                          , [Twitter] TEXT NOT NULL
                          , [Instagram] TEXT NOT NULL
                          , [Interests] TEXT NOT NULL
                          , [IsNsfw] INTEGER NOT NULL
                          , [IsActive] INTEGER NOT NULL
                          , [Description] TEXT NOT NULL
                          , [ProfileId] INTEGER NOT NULL
                          , CONSTRAINT[PK_profiles] PRIMARY KEY([Id])
                       );
                ";

                var createIndexCommandContents = connection.CreateCommand();
                createIndexCommandContents.CommandText = "CREATE UNIQUE INDEX IF NOT EXISTS index_username ON profiles(Username);";

                var createIndexIdCommandContents = connection.CreateCommand();
                createIndexIdCommandContents.CommandText = "CREATE UNIQUE INDEX IF NOT EXISTS index_profileId ON profiles(ProfileId);";

                var createTableCommandRelateds = connection.CreateCommand();
                createTableCommandRelateds.CommandText = @"
                        CREATE TABLE IF NOT EXISTS [relateds] (
                            [Id] INTEGER NOT NULL
                          , [ProfileId] INTEGER NOT NULL
                          , [CategoryId] INTEGER NOT NULL
                          , [FirstName] TEXT NOT NULL
                          , [LastName] TEXT NOT NULL
                          , [Username] TEXT NOT NULL
                          , [BoorCode] TEXT NOT NULL
                          , [OverlayPath] TEXT NOT NULL
                          , [Interests] TEXT NOT NULL
                          , [About] TEXT NOT NULL
                          , CONSTRAINT[PK_profiles] PRIMARY KEY([Id])
                       );
                ";

                var createIndexCommandRelateds = connection.CreateCommand();
                createIndexCommandRelateds.CommandText = "CREATE INDEX IF NOT EXISTS index_profile_id ON relateds(ProfileId);";

                try
                {
                    createTableCommandContents.ExecuteNonQuery();
                    createIndexCommandContents.ExecuteNonQuery();
                    createIndexIdCommandContents.ExecuteNonQuery();

                    createTableCommandRelateds.ExecuteNonQuery();
                    createIndexCommandRelateds.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private SqliteConnection GetConnection()
        {
            return new SqliteConnection($"Data Source={dbFilePath}");
        }

        private Model.Profile ReaderToContentMap(SqliteDataReader reader)
        {
            var profile = new Model.Profile();

            profile.Id = reader.GetInt32(reader.GetOrdinal("ProfileId"));
            profile.About = reader.GetString(reader.GetOrdinal("About"));
            profile.BoorCode = reader.GetString(2);
            profile.CategoryId = reader.GetInt32(3);
            var publishDateStr = reader.GetString(4);
            profile.BirthDate = DateTime.Parse(publishDateStr);
            profile.Email = reader.GetString(5);
            profile.FirstName = reader.GetString(6);
            profile.LastName = reader.GetString(7);
            profile.OverlayPath = reader.GetString(8);
            profile.Pinterest = reader.GetString(9);
            profile.Username = reader.GetString(10);
            profile.TotalLike = reader.GetInt32(11);
            profile.Youtube = reader.GetString(12);
            profile.Facebook = reader.GetString(13);
            profile.Twitter = reader.GetString(14);
            profile.Instagram = reader.GetString(15);
            profile.Interests = reader.GetString(16);
            profile.IsNsfw = reader.GetBoolean(17);
            profile.Description = reader.GetString(19);

            return profile;
        }

        private Model.Dto.ProfileForListSimpleDto ReaderToContentSimple(SqliteDataReader reader)
        {
            var related = new ProfileForListSimpleDto();

            related.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
            related.LastName = reader.GetString(reader.GetOrdinal("LastName"));
            related.BoorCode = reader.GetString(reader.GetOrdinal("BoorCode"));
            related.ProfileId = reader.GetInt32(reader.GetOrdinal("ProfileId"));
            related.Username = reader.GetString(reader.GetOrdinal("Username"));
            related.OverlayPath = reader.GetString(reader.GetOrdinal("OverlayPath"));
            related.Interests = reader.GetString(reader.GetOrdinal("Interests"));
            related.About = reader.GetString(reader.GetOrdinal("About"));
            related.CategoryId = reader.GetInt32(reader.GetOrdinal("CategoryId"));

            return related;
        }
    }

    public class ProfileDbCacheDto
    {
        public ProfileDbCacheDto()
        {
            Profile = null;
            RelatedProfiles = new List<ProfileForListSimpleDto>();
        }

        public int ProfileId { get; set; }
        public int CategoryId { get; set; }
        public Model.Profile Profile { get; set; }
        public List<Model.Dto.ProfileForListSimpleDto> RelatedProfiles { get; set; }
    }
}
