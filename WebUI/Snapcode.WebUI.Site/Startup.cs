using Snapcode.Data;
using Snapcode.Data.Infrastructure.Entities;
using Snapcode.Infrastructure.Interfaces;
using Snapcode.WebUI.Infrastructure.Helpers;
using Snapcode.WebUI.Infrastructure.Media;
using Snapcode.WebUI.Infrastructure.Rules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Snapcode.WebUI.Site.Cache;
using Snapcode.WebUI.Site.Jobs;
using Hangfire;
using Hangfire.MemoryStorage;

namespace Snapcode.WebUI.Site
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IConfigurationRoot ConfigurationRoot { get; set; }
        public Microsoft.AspNetCore.Hosting.IHostingEnvironment Environment { get; set; }

        public Startup(IConfiguration configuration, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;

            ConfigurationRoot = new ConfigurationBuilder()
                            .SetBasePath(env.ContentRootPath)
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                            .AddEnvironmentVariables()
                            .Build();

        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (Environment.IsDevelopment())
                services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddHangfire(config =>
            {
                config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                      .UseSimpleAssemblyNameTypeSerializer()
                      .UseDefaultTypeSerializer()
                      .UseMemoryStorage();

                config.UseFilter(new AutomaticRetryAttribute { Attempts = 0 });
            }

            );
            services.AddHangfireServer();

            //Config

            services.Configure<DatabaseSettings>(Configuration.GetSection("DatabaseSettings"));
            services.AddOptions();

            var databaseSettings = Configuration
                          .GetSection("DatabaseSettings")
                          .Get<DatabaseSettings>();
            services.AddScoped(x => new DataContext(databaseSettings.ConnectionString));
            services.AddDbContext<DataContext>(ServiceLifetime.Scoped);

            //Cache
            services.AddMemoryCache();
            services.AddTransient<ICache, Snapcode.Infrastructure.Caching.MemoryCache.Cache>();
            services.AddTransient<Infrastructure.Cache.CacheHelper>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.Strict;
            });


            services.AddTransient<AdminData>();
            services.AddTransient<BanwordData>();
            services.AddTransient<CategoryData>();
            services.AddTransient<HashtagData>();
            services.AddTransient<ListData>();
            services.AddTransient<PopularData>();
            services.AddTransient<ProfileData>();
            services.AddTransient<RemovalData>();
            services.AddTransient<ReviewData>();
            services.AddTransient<SettingData>();
            services.AddTransient<AdsAccountData>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddTransient<MediaHelper>();
            services.AddTransient<HtmlHeadHelpers>();
            services.AddTransient<SmsHelper>();
            services.AddTransient<SeoHelper>();
            services.AddTransient<Cache.ProfileDbCacheHelper>();

            services.AddMvc(x =>
            {
                x.EnableEndpointRouting = false;
            });

            services.Configure<RouteOptions>(routeOptions => routeOptions.AppendTrailingSlash = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app
            , IWebHostEnvironment env
            , IRecurringJobManager recurringJobManager
            , ProfileDbCacheHelper contentDbCacheHelper)
        {
            if (env.IsDevelopment()) { app.UseStatusCodePages(); }

            app.UseDeveloperExceptionPage();

            RedirectToHttpsWwwNonWwwRule rule = new RedirectToHttpsWwwNonWwwRule
            {
                status_code = 301,
                redirect_to_https = true,
                redirect_to_www = false,
                redirect_to_non_www = true,
                append_slash = true
            };
            RewriteOptions options = new RewriteOptions();
            options.Rules.Add(rule);
            app.UseRewriter(options);

            contentDbCacheHelper.InitTables();

            app.UseRouting();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "category", template: "category/{slug}", defaults: new { controller = "Category", action = "Index", page = 1 });
                routes.MapRoute(name: "categoryWithPage", template: "category/{slug}/page/{page}", defaults: new { controller = "Category", action = "Index", page = 1 });
                routes.MapRoute(name: "tag", template: "hashtag/{slug}", defaults: new { controller = "Hashtag", action = "Index", page = 1 });
                routes.MapRoute(name: "tagWithPage", template: "hashtag/{slug}/page/{page}", defaults: new { controller = "Hashtag", action = "Index", page = 1 });
                routes.MapRoute(name: "list", template: "list/{slug}", defaults: new { controller = "List", action = "Index", page = 1 });
                routes.MapRoute(name: "listWithPage", template: "list/{slug}/page/{page}", defaults: new { controller = "List", action = "Index", page = 1 });
                routes.MapRoute(name: "Remove", template: "remove-me", defaults: new { controller = "Remove", action = "Index" });
                routes.MapRoute(name: "Add", template: "add-me", defaults: new { controller = "Profile", action = "Add" });
                routes.MapRoute(name: "About", template: "page/about", defaults: new { controller = "Page", action = "About" });
                routes.MapRoute(name: "Privacy", template: "page/privacy", defaults: new { controller = "Page", action = "Privacy" });
                routes.MapRoute(name: "profile", template: "profile/{username}", defaults: new { controller = "Profile", action = "Index" });
                routes.MapRoute(name: "popular", template: "popular/{username}", defaults: new { controller = "Popular", action = "Index" });
                routes.MapRoute(name: "search", template: "search/{query}", defaults: new { controller = "Search", action = "Index" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
