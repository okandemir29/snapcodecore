﻿using Hangfire;
using Snapcode.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Jobs
{
    public class ProfileMigrateToCacheDbJob
    {
        ProfileData profileData;
        Cache.ProfileDbCacheHelper profileDbCacheHelper;

        public ProfileMigrateToCacheDbJob(ProfileData profileData, Cache.ProfileDbCacheHelper profileDbCacheHelper)
        {
            this.profileData = profileData;
            this.profileDbCacheHelper = profileDbCacheHelper;
        }

        [AutomaticRetry(Attempts = 0, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        public void Execute()
        {
            //db'yi hazirla
            profileDbCacheHelper.DropTables();
            profileDbCacheHelper.InitTables();

            var profiles = profileData.GetBy(x => x.IsActive);
            var counter = 0;
            foreach (var content in profiles)
            {
                counter += 1;
                if (counter >= 1000)
                    break;

                var relatedContents = profiles.Where(x => x.CategoryId == content.CategoryId).Select(x => new Model.Dto.ProfileForListSimpleDto()
                {
                    BoorCode = x.BoorCode,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    OverlayPath = x.OverlayPath,
                    ProfileId = x.Id,
                    Username = x.Username,
                }).ToList();

                profileDbCacheHelper.Insert(content, relatedContents);
            }
        }
    }
}
