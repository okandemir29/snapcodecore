var search_opener = document.getElementById("searchOpener");
var search_close = document.getElementsByClassName("search-close")[0];
var menu_opener = document.getElementById("menuOpener");
var menu_close = document.getElementById("closeMenu");


let menu_search_button = document.getElementById("msButton");
let menu_search_input = document.getElementById("msInput");
let aside_search_button = document.getElementById("asideButton");
let aside_search_input = document.getElementById("asideInput");

if (aside_search_button != undefined) {
    aside_search_button.addEventListener("click", search);
}
if (aside_search_input != undefined) {
    aside_search_input.addEventListener("keyup", searchWithKeyboard);
}

menu_search_button.addEventListener("click", search);
menu_search_input.addEventListener("keyup", searchWithKeyboard);

search_opener.addEventListener("click", openSearch);
search_close.addEventListener("click", closeSearch);
menu_opener.addEventListener("click", openMenu);
menu_close.addEventListener("click", closeMenu);

function openSearch() {
    document.getElementsByClassName("search-container")[0].classList.add("search-active");
}
function closeSearch() {
    document.getElementsByClassName("search-active")[0].classList.remove("search-active");
}

function openMenu() {
    document.getElementsByClassName("menu-container")[0].classList.add("menu-active");
}
function closeMenu() {
    document.getElementsByClassName("menu-active")[0].classList.remove("menu-active");
}

function search() {
    var search_text = menu_search_input.value;
    if (search_text.length == 0) {
        let aside_search = aside_search_input.value;
        if (aside_search.length <= 3) alert("too short query");
        else location.href = "/search/" + aside_search;

        return false;
    }
    if (search_text.length <= 3) alert("too short query");
    else location.href = "/search/" + search_text;
}

function searchWithKeyboard(e) {
    var search_text = menu_search_input.value;
    if (e.keyCode == 13) {
        if (search_text.length == 0) {
            let aside_search = aside_search_input.value;
            if (aside_search.length <= 3) alert("too short query");
            else location.href = "/search/" + aside_search;
            return false;
        }
        if (search_text.length <= 3) alert("too short query");
        else location.href = "/search/" + search_text;
    }
}

var lazyElementsInPictures = [].slice.call(document.querySelectorAll("picture > source, picture > img"));
var lazyImagesNotInPictures = [].slice.call(document.querySelectorAll('img[lazy-src]'));

if ("IntersectionObserver" in window) {
  let lazyInPictureElementsObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
       if (entry.isIntersecting) {      
          let lazyImage = entry.target;
		  
		  if(lazyImage.tagName === 'IMG' && lazyImage.dataset.src != undefined){ lazyImage.src = lazyImage.dataset.src; }
		  else if (lazyImage.dataset.srcset != undefined){ lazyImage.srcset = lazyImage.dataset.srcset; }
          
		  lazyImage.parentElement.classList.remove("lazy");
          lazyInPictureElementsObserver.unobserve(lazyImage);
        }
     });
  });
  lazyElementsInPictures.forEach(img => lazyInPictureElementsObserver.observe(img)); 
  
   var lazyNotInPictureImgObserver = new IntersectionObserver(function (entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                let lazyImage = entry.target;
                
                var dataSrc = lazyImage.getAttribute('lazy-src');
                
                lazyImage.setAttribute('src', dataSrc);
                lazyImage.setAttribute('lazy-src', '');
                
                lazyNotInPictureImgObserver.unobserve(lazyImage);
            }
        });
    });
    lazyImagesNotInPictures.forEach(img => lazyNotInPictureImgObserver.observe(img));
} 
else {
    lazyElementsInPictures.forEach(function(image){ image.nextElementSibling.src = image.nextElementSibling.dataset.srcset; });
    lazyImagesNotInPictures.forEach(function(image){ image.nextElementSibling.src = image.nextElementSibling.dataset.srcset; });
}

const getCookieValue = (name) => (
    document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || ''
)

document.getElementById("accept-cookie").addEventListener("click", acceptCookie);
document.getElementById("close-cookie").addEventListener("click", closeCookie);

function acceptCookie() {
    document.cookie = "accept-terms=ok";
    document.getElementsByClassName("cookie-box")[0].style.display = "none";
}

function closeCookie() {
    document.getElementsByClassName("cookie-box")[0].style.display = "none";
}

window.addEventListener('DOMContentLoaded', (event) => {
    var value = getCookieValue("accept-terms");
    if (value == "ok") {
        document.getElementsByClassName("cookie-box")[0].style.display = "none";
    }
    else {
        document.getElementsByClassName("cookie-box")[0].style.display = "block";
        document.getElementsByClassName("cookie-box")[0].style.display = "none";
    }
});

function offset(elt) {
    var rect = elt.getBoundingClientRect(), bodyElt = document.body;
    return {
        top: rect.top + bodyElt.scrollTop,
        left: rect.left + bodyElt.scrollLeft
    }
}

window.addEventListener("load", function () {
    if (document.querySelector("#sidebar")) {
        const sidebar = document.querySelector("#sidebar");
        const footer = document.querySelector("footer");
        const top = offset(sidebar).top;
        const footTop = offset(footer).top;
        const maxY = footTop - sidebar.offsetHeight

        window.addEventListener("scroll", function () {
            let y = document.scrollingElement.scrollTop;
            if (y > 200) {
                if (y < maxY) {
                    sidebar.classList.add("fixed")
                    sidebar.removeAttribute('style');
                }

            }
            else {
                sidebar.classList.remove("fixed")
            }
        })
    }
});