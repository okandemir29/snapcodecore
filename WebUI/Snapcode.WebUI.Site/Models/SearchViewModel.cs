﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            Profiles = new List<Model.Profile>();
            Hashtags = new List<Model.Hashtag>();
        }

        public List<Model.Profile> Profiles { get; set; }
        public List<Model.Hashtag> Hashtags { get; set; }
        public string Query { get; set; }
    }
}
