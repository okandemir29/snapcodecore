﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class ListViewModel
    {
        public ListViewModel()
        {
            List = new Model.List();
            Populars = new List<Model.Popular>();
        }

        public Model.List List { get; set; }
        public List<Model.Popular> Populars { get; set; }

        public int TotalCount { get; set; }
        public int TotalPage { get; set; }
        public int Page { get; set; }
    }
}
