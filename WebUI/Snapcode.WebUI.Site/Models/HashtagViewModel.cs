﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class HashtagViewModel
    {
        public HashtagViewModel()
        {
            Hashtag = new Model.Hashtag();
            Profiles = new List<Model.Profile>();
        }

        public Model.Hashtag Hashtag { get; set; }
        public List<Model.Profile> Profiles { get; set; }

        public string Tag { get; set; }
        public int TotalCount { get; set; }
        public int TotalPage { get; set; }
        public int Page { get; set; }
    }
}
