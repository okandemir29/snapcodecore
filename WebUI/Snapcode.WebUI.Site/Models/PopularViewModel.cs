﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class PopularViewModel
    {
        public PopularViewModel()
        {
            Relateds = new List<Model.Popular>();
            Reviews = new List<Model.Review>();
            Profile = new Model.Popular();
        }

        public Model.Popular Profile { get; set; }
        public List<Model.Popular> Relateds { get; set; }
        public List<Model.Review> Reviews { get; set; }
    }
}
