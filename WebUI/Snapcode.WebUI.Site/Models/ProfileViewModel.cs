﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class ProfileViewModel
    {
        public ProfileViewModel()
        {
            Relateds = new List<Model.Dto.ProfileForListSimpleDto>();
            Reviews = new List<Model.Review>();
            Profile = new Model.Profile();
        }

        public Model.Profile Profile { get; set; }
        public List<Model.Dto.ProfileForListSimpleDto> Relateds { get; set; }
        public List<Model.Review> Reviews { get; set; }
    }
}
