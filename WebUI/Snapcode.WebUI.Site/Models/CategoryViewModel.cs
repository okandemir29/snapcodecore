﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Category = new Model.Category();
            Profiles = new List<Model.Profile>();
        }

        public Model.Category Category { get; set; }
        public List<Model.Profile> Profiles { get; set; }

        public int TotalCount { get; set; }
        public int TotalPage { get; set; }
        public int Page { get; set; }
    }
}
