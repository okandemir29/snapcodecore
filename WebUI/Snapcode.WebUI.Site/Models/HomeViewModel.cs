﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Snapcode.WebUI.Site.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            Profiles = new List<Model.Profile>();
        }

        public List<Model.Profile> Profiles { get; set; }
    }
}
