﻿using Microsoft.AspNetCore.Mvc.Filters;
using Snapcode.Data;
using Snapcode.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Snapcode.WebUI.Infrastructure.Cache
{
    public class CacheHelper
    {
        ICache cache;
        PopularData popularData;
        ListData listData;
        CategoryData categoryData;
        ProfileData profileData;
        SettingData settingData;
        AdsAccountData adsAccountData;

        public CacheHelper(ICache cache
            , PopularData popularData
            , ListData listData
            , CategoryData categoryData
            , ProfileData profileData
            , SettingData settingData
            , AdsAccountData adsAccountData
            )
        {
            this.cache = cache;
            this.popularData = popularData;
            this.listData = listData;
            this.categoryData = categoryData;
            this.profileData = profileData;
            this.settingData = settingData;
            this.adsAccountData = adsAccountData;
        }

        private string PopularAccounts_CacheKey = "PopularAccounts_CacheKey";
        public bool PopularAccountClear() { return Clear(PopularAccounts_CacheKey); }
        public List<Model.Popular> PopularAccounts
        {
            get
            {
                var fromCache = Get<List<Model.Popular>>(PopularAccounts_CacheKey);
                if (fromCache == null)
                {
                    var datas = popularData.GetAll();
                    if (datas != null)
                    {
                        Set(PopularAccounts_CacheKey, datas);
                        fromCache = datas;
                    }
                        
                }

                return fromCache;
            }
        }

        private string Lists_CacheKey = "Lists_CacheKey";
        public bool ListsClear() { return Clear(Lists_CacheKey); }
        public List<Model.List> Lists
        {
            get
            {
                var fromCache = Get<List<Model.List>>(Lists_CacheKey);
                if (fromCache == null)
                {
                    var datas = listData.GetAll();
                    if (datas != null)
                    {
                        Set(Lists_CacheKey, datas);
                        fromCache = datas;
                    }

                }

                return fromCache;
            }
        }

        private string Categories_CacheKey = "Categories_CacheKey";
        public bool CategoriesClear() { return Clear(Categories_CacheKey); }
        public List<Model.Category> Categories
        {
            get
            {
                var fromCache = Get<List<Model.Category>>(Categories_CacheKey);
                if (fromCache == null)
                {
                    var datas = categoryData.GetBy(x=>x.IsActive);
                    if (datas != null)
                    {
                        Set(Categories_CacheKey, datas);
                        fromCache = datas;
                    }
                }

                return fromCache;
            }
        }

        private string Profiles_CacheKey = "Profiles_CacheKey";
        public bool ProfilesClear() { return Clear(Profiles_CacheKey); }
        public List<Model.Profile> Profiles
        {
            get
            {
                var fromCache = Get<List<Model.Profile>>(Profiles_CacheKey);
                if (fromCache == null)
                {
                    var datas = profileData.GetByPage(x=>x.IsActive && !x.IsNsfw, 1, 30, "Id", true);
                    if (datas != null)
                    {
                        Set(Profiles_CacheKey, datas);
                        fromCache = datas;
                    }

                }

                return fromCache;
            }
        }

        private string Setting_CacheKey = "Setting_CacheKey";
        public bool SettingClear() { return Clear(Setting_CacheKey); }
        public Model.Setting Setting
        {
            get
            {
                var fromCache = Get<Model.Setting>(Setting_CacheKey);
                if (fromCache == null)
                {
                    var datas = settingData.GetAll().FirstOrDefault();
                    if (datas != null)
                    {
                        Set(Setting_CacheKey, datas);
                        fromCache = datas;
                    }

                }

                return fromCache;
            }
        }

        private string AdsAccount_CacheKey = "AdsAccount_CacheKey";
        public bool AdsAccountClear() { return Clear(AdsAccount_CacheKey); }
        public Model.AdsAccount AdsAccounts
        {
            get
            {
                var fromCache = Get<Model.AdsAccount>(AdsAccount_CacheKey);
                if (fromCache == null)
                {
                    var datas = adsAccountData.GetBy(x=>x.HasAds).FirstOrDefault();
                    if (datas != null)
                    {
                        Set(AdsAccount_CacheKey, datas);
                        fromCache = datas;
                    }
                }

                return fromCache;
            }
        }

        public bool Clear(string name)
        {
            cache.Remove(name);

            return true;
        }

        public T Get<T>(string cacheKey) where T : class
        {
            object cookies;

            if (!cache.TryGetValue(cacheKey, out cookies))
                return null;

            return cookies as T;
        }

        public void Set(string cacheKey, object value)
        {
            cache.Set(cacheKey, value, 180);
        }
    }
}
