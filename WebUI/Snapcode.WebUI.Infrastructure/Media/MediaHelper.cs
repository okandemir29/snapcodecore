﻿using ImageMagick;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Snapcode.WebUI.Infrastructure.Media
{
    public class MediaHelper
    {
        IHostingEnvironment hostEnvironment;

        public MediaHelper(IHostingEnvironment hostEnvironment)
        {
            this.hostEnvironment = hostEnvironment;
        }

        public string GetCroppedMedia(string path, int width, int height)
        {
            string contentRootPath = hostEnvironment.ContentRootPath + "\\wwwroot\\";

            path = contentRootPath + path.TrimStart('/');
            var directory = Path.GetDirectoryName(path);
            var fileName = Path.GetFileNameWithoutExtension(path);
            var extension = Path.GetExtension(path).TrimStart('.');
            var requestedFileName = $"{fileName}-{width}x{height}";
            var requestedFilePath = $"{directory}\\{requestedFileName}.{extension}";

            //gelen dosya yok ki :(
            if (!File.Exists(path))
                return "";

            if (!File.Exists(requestedFilePath))
            {
                using (var image = new MagickImage(path))
                {
                    image.Resize(width, height);
                    image.Crop(width, height);
                    image.Write(requestedFilePath);
                }
            }

            var relativeRequestedFilePath = "\\" + requestedFilePath.Replace(contentRootPath, "");
            return $"https://okandemir.com{relativeRequestedFilePath}";
        }
    }
}
