﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Snapcode.WebUI.Infrastructure.Helpers
{
    public class SmsHelper
    {
        public void SendSms(string message, string number, string title = "OKAN DEMIR")
        {
            if (string.IsNullOrEmpty(number))
            {
                return;
            }
            if (number.Length < 10)
            {
                return;
            }

            var smsTitle = "OKAN DEMIR";
            if (!string.IsNullOrEmpty(title) && title.Length < 30 && title.Length > 1)
            {
                smsTitle = title;
            }

            string test = HTTPPoster(
                "<SingleTextSMS>" +
                "<UserName>OKANDEMIR-7214</UserName>" +
                "<PassWord>123456</PassWord>" +
                "<Action>0</Action>" +
                "<Mesgbody>" + message + "</Mesgbody>" +
                "<Numbers>" + number + "</Numbers>" +
                "<Originator>" + smsTitle + "</Originator>" +
                "<SDate></SDate>" +
                "<ExDate></ExDate>" +
                "</SingleTextSMS>"
                );
        }

        private string HTTPPoster(string prmSendData)
        {
            try
            {
                WebClient wUpload = new WebClient();
                wUpload.Proxy = null;
                Byte[] bPostArray = Encoding.UTF8.GetBytes(prmSendData);
                Byte[] bResponse = wUpload.UploadData("http://g3.iletimx.com", "POST", bPostArray);
                Char[] sReturnChars = Encoding.UTF8.GetChars(bResponse);
                string sWebPage = new string(sReturnChars);
                return sWebPage;
            }
            catch
            {
                return "-1";
            }
        }
    }
}
