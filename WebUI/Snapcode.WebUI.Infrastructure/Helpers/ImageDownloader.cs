﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;
using Snapcode.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace Snapcode.WebUI.Infrastructure.Helpers
{
    public class ImageDownloader
    {
        private HashHelper hashHelper;

        IHostingEnvironment hostEnvironment;
        public ImageDownloader(IHostingEnvironment hostEnvironment)
        {
            this.hostEnvironment = hostEnvironment;
            this.hashHelper = new HashHelper();
        }

        public string DownloadPostInImages(string value, string _basePath, string url, List<string> replace_rules)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(value);
            var imageNodes = doc.DocumentNode.SelectNodes("//img//@src");
            if (imageNodes == null || imageNodes.Count == 0)
                return value;

            foreach (HtmlNode imageNode in imageNodes)
            {
                var remote_media_path = imageNode.Attributes["src"].Value;
                var local_media_path = remote_media_path.Replace(_basePath, "").TrimStart('/');
                var rootFolder = Directory.GetCurrentDirectory();
                string contentRootPath = hostEnvironment.ContentRootPath + "\\wwwroot\\";
                string filePath = "wp-content\\uploads";

                var path = local_media_path.Split('/');
                var yearFile = path[2];
                var monthFile = path[3];

                if (!Directory.Exists($"{contentRootPath}{filePath}\\{yearFile}"))
                    Directory.CreateDirectory($"{contentRootPath}{filePath}\\{yearFile}");

                if (!Directory.Exists($"{contentRootPath}{filePath}\\{yearFile}\\{monthFile}"))
                    Directory.CreateDirectory($"{contentRootPath}{filePath}\\{yearFile}\\{monthFile}");

                var fileName = path.LastOrDefault();
                var resultPath = $"{contentRootPath}{filePath}\\{yearFile}\\{monthFile}\\{fileName}";

                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36");
                        wc.Headers.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");

                        wc.DownloadFile(remote_media_path, resultPath);

                        var newSrc = $"{filePath}/{yearFile}/{monthFile}/{fileName}";
                        newSrc = newSrc.Replace("\\", "/");
                        newSrc = url + "/" + newSrc;

                        //imagenode'un attr'leri degistirelim
                        imageNode.SetAttributeValue("src", newSrc);

                        if (imageNode.Attributes["alt"] != null)
                        {
                            var alt = imageNode.Attributes["alt"].Value;
                            imageNode.SetAttributeValue("alt", alt.ToExecuteReplaceRules(replace_rules));
                        }

                        if (imageNode.Attributes["title"] != null)
                        {
                            var title = imageNode.Attributes["title"].Value;
                            imageNode.SetAttributeValue("title", title.ToExecuteReplaceRules(replace_rules));
                        }

                        if (imageNode.Attributes["srcset"] != null)
                            imageNode.Attributes.Remove(imageNode.Attributes["srcset"]);

                        hashHelper.ChangeHash(resultPath);
                    }
                }
                catch (Exception ex)
                {
                }
            }

            using (StringWriter writer = new StringWriter())
            {
                doc.Save(writer);
                value = writer.ToString();
            }

            return value;
        }
    }
}
