﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snapcode.WebUI.Infrastructure.Helpers
{
    public class SeoHelper
    {
        public string PopularUserImage(string username, string fullname)
        {
            var text = $"{fullname}'s snapchat profile stories and snapcode on {username}";
            return text;
        }

        public string PopularUserHref(string username, string fullname)
        {
            var text = $"{fullname}'s snapchat profile stories and snapcode on {username} discover popular user lists";
            return text;
        }

        public string ProfileUserImage(string username, string fullname, string about)
        {
            var text = $"{fullname}'s snapchat profile stories and snapcode on {username}, {about}";
            return text;
        }

        public string ProfileUserHref(string username, string fullname)
        {
            var text = $"{fullname}'s snapchat profile stories and snapcode on {username}, discover snapchat users";
            return text;
        }

        public string ListHrefText(string name)
        {
            var text = $"Popular snapchat users and snapcode's {name} list in sortable easy discover";
            return text;
        }

        public string CategoryHrefText(string name)
        {
            var text = $"Categorized snapchat users with snapcode's on {name} category";
            return text;
        }

        public string HashtagHrefText(string name)
        {
            var text = $"Interest #{name} snapchat users and discover new friends make a easy followers";
            return text;
        }
    }
}
