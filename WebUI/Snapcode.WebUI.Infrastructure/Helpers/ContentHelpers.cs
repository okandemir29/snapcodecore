﻿namespace Snapcode.WebUI.Infrastructure.Helpers
{
    using HtmlAgilityPack;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class ContentHelpers
    {
        public static string CreateNavigation(string content)
        {
            HtmlDocument htmldoc = new HtmlDocument();
            htmldoc.LoadHtml(content);
            var h_tags = htmldoc.DocumentNode.SelectNodes("//h2 | //h3 | //h4 | //h5 | //h6");
            int navCounter = 1;
            if (h_tags != null && h_tags.Count > 2)
            {
                var content_navigation = new StringBuilder("<div class='content-navigation'><ul>");

                foreach (var item in h_tags)
                {
                    item.Attributes.Add("id", "navigation-" + navCounter);
                    content_navigation.Append($"<li><a href='#navigation-{navCounter}'>{item.InnerText}</a></li>");

                    navCounter++;
                }

                content_navigation.Append("</ul></div>");

                htmldoc.DocumentNode.PrependChild(HtmlNode.CreateNode(content_navigation.ToString()));
            }

            return htmldoc.DocumentNode.OuterHtml;
        }
    }
}
