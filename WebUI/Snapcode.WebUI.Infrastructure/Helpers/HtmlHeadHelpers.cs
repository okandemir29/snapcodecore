﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Snapcode.WebUI.Infrastructure.Cache;

namespace Snapcode.WebUI.Infrastructure.Helpers
{
    public class HtmlHeadHelpers
    {
        Cache.CacheHelper cacheHelper;
        IActionContextAccessor actionContextAccessor { get; set; }

        public HtmlHeadHelpers(CacheHelper cacheHelper
            , IActionContextAccessor actionContextAccessor)
        {
            this.cacheHelper = cacheHelper;
            this.actionContextAccessor = actionContextAccessor;
        }

        public string Canonical(string action, string controller, object values = null)
        {
            var urlHelper = new UrlHelper(actionContextAccessor.ActionContext);
            var url = "https://spanbear.com";
            var canonical = url.TrimEnd('/')
                + urlHelper.Action(new UrlActionContext() { 
                    Controller = controller,
                    Action = action,
                    Values = values
                });

            return canonical;
        }
    }
}
