﻿using Microsoft.Extensions.DependencyInjection;
using Snapcode.Data;
using Snapcode.Data.Infrastructure.Entities;
using System;
using System.IO;
using System.Linq;
using System.Net;

namespace Snapcode.Bots.ImageProcess
{
    class Program
    {
        static ServiceProvider serviceProvider;

        static void Main(string[] args)
        {
            serviceProvider = new ServiceCollection()
             .AddOptions()
             .AddSingleton<ProfileData>()
             .AddScoped<DataContext>(x => new DataContext("server=116.202.53.25;port=3306;database=u9243176_dbGhost;userId=u9243176_usGhost;Password='CSch37D8EFwg08Q'"))
              .AddDbContext<DataContext>(ServiceLifetime.Scoped)
             .Configure<DatabaseSettings>(options
                 => options.ConnectionString = "server=116.202.53.25;port=3306;database=u9243176_dbGhost;userId=u9243176_usGhost;Password='CSch37D8EFwg08Q'")
              .BuildServiceProvider();

            var _profileData = serviceProvider.GetService<ProfileData>();

            var ok = 0;
            var no = 0;

            for (int i = 150; i < 230; i++)
            {
                var profiles = _profileData.GetByPage(x => x.IsActive, i, 1000);
                foreach (var profile in profiles)
                {
                    var remote_path = "";
                    if (!string.IsNullOrEmpty(profile.BoorCode))
                        remote_path = profile.BoorCode;
                    else
                        remote_path = profile.OverlayPath;

                    if (string.IsNullOrEmpty(remote_path))
                    {
                        profile.OverlayPath = "/_assets/images/unknown.png";
                        profile.BoorCode = "/_assets/images/unknown.png";
                        _profileData.Update(profile);
                        continue;
                    }

                    var extension = Path.GetExtension(remote_path);
                    var local_media_path = $"images/{profile.Username}{extension}";

                    if (File.Exists(local_media_path))
                        continue;

                    if (!Directory.Exists(Path.GetDirectoryName(local_media_path)))
                        Directory.CreateDirectory(Path.GetDirectoryName(local_media_path));

                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(remote_path, local_media_path);
                            ok++;
                            Console.WriteLine($"{profile.Username} ile indirdim ok-{ok}");
                        }
                    }
                    catch (Exception ex)
                    {
                        no++;
                        Console.WriteLine($"{profile.Username} resmini indiremedim no-{no}");
                    }
                }
            }

            Console.WriteLine("İşlemlerimi bitirdim");
            Console.ReadLine();
        }
    }
}
