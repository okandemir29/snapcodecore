﻿namespace Snapcode.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Snapcode.Data.Infrastructure;
    using Snapcode.Infrastructure.Interfaces;

    public class SettingData : EntityBaseData<Model.Setting>
    {
        public SettingData(IServiceScopeFactory serviceScopeFactory)
          : base(serviceScopeFactory) { }
    }
}
