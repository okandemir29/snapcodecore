﻿namespace Snapcode.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Snapcode.Data.Infrastructure;
    using Snapcode.Infrastructure.Interfaces;

    public class ProfileData : EntityBaseData<Model.Profile>
    {
        public ProfileData(IServiceScopeFactory serviceScopeFactory)
          : base(serviceScopeFactory) { }
    }
}
