﻿namespace Snapcode.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Snapcode.Data.Infrastructure;
    using Snapcode.Infrastructure.Interfaces;

    public class CategoryData : EntityBaseData<Model.Category>
    {
        public CategoryData(IServiceScopeFactory serviceScopeFactory)
          : base(serviceScopeFactory) { }
    }
}
