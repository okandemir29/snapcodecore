﻿namespace Snapcode.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Snapcode.Data.Infrastructure;
    using Snapcode.Infrastructure.Interfaces;

    public class HashtagData : EntityBaseData<Model.Hashtag>
    {
        public HashtagData(IServiceScopeFactory serviceScopeFactory)
          : base(serviceScopeFactory) { }
    }
}
