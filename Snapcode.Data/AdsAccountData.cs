﻿namespace Snapcode.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Snapcode.Data.Infrastructure;
    using Snapcode.Infrastructure.Interfaces;

    public class AdsAccountData : EntityBaseData<Model.AdsAccount>
    {
        public AdsAccountData(IServiceScopeFactory serviceScopeFactory)
          : base(serviceScopeFactory) { }
    }
}
