﻿namespace Snapcode.Data
{
    using Microsoft.EntityFrameworkCore;
    using Snapcode.Data.Infrastructure;

    public class DataContext : DbContext
    {
        public DataContext(string connectionString)
            : base(new DbContextOptionsBuilder().UseMySQL(connectionString).Options)
        {
        }

        public DbSet<Model.Admin> Admins { get; set; }
        public DbSet<Model.Banword> Banwords { get; set; }
        public DbSet<Model.Category> Categories { get; set; }
        public DbSet<Model.Hashtag> Hashtags { get; set; }
        public DbSet<Model.List> Lists { get; set; }
        public DbSet<Model.Popular> Populars { get; set; }
        public DbSet<Model.Profile> Profiles { get; set; }
        public DbSet<Model.Removal> Removals { get; set; }
        public DbSet<Model.Review> Reviews { get; set; }
        public DbSet<Model.Setting> Settings { get; set; }
        public DbSet<Model.AdsAccount> AdsAccounts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Model.Admin>(entity => entity.ToTable("gh_admins"));
            builder.Entity<Model.Banword>(entity => entity.ToTable("gh_banwords"));
            builder.Entity<Model.Category>(entity => entity.ToTable("gh_categories"));
            builder.Entity<Model.Hashtag>(entity => entity.ToTable("gh_hashtags"));
            builder.Entity<Model.List>(entity => entity.ToTable("gh_lists"));
            builder.Entity<Model.Popular>(entity => entity.ToTable("gh_populars"));
            builder.Entity<Model.Profile>(entity => entity.ToTable("gh_profiles"));
            builder.Entity<Model.Removal>(entity => entity.ToTable("gh_removals"));
            builder.Entity<Model.Review>(entity => entity.ToTable("gh_reviews"));
            builder.Entity<Model.Setting>(entity => entity.ToTable("gh_settings"));
            builder.Entity<Model.AdsAccount>(entity => entity.ToTable("gh_ads_accounts"));

            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(bool))
                    {
                        property.SetValueConverter(new BoolToIntConverter());
                    }
                }
            }
        }
    }
}