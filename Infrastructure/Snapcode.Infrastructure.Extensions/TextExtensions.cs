﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace Snapcode.Infrastructure.Extensions
{
    public static class TextExtensions
    {
        public static string ToStripHtml(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            return Regex.Replace(value, "<.*?>", String.Empty);
        }

        public static string ToExecuteReplaceRules(this string value, List<string> replaceRules)
        {
            replaceRules.ForEach(x =>
            {
                value = value.Replace(x, "");
            });

            return value;
        }

        public static string ToRemoveAnchors(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            return Regex.Replace(value, @"<a\b[^>]+>([^<]*(?:(?!</a)<[^<]*)*)</a>", "$1");
        }

        public static string RemoveUnicodeCharacter(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            value = value.Replace(@"\ç", "ç")
                        .Replace(@"\u00e7", "ç")
                        .Replace(@"\u00c7", "Ç")
                        .Replace(@"\Ç", "Ç")
                        .Replace(@"\u015f", "ş")
                        .Replace(@"\ş", "ş")
                        .Replace(@"\u0131", "ı")
                        .Replace(@"\ı", "ı")
                        .Replace(@"\u022b", "ö")
                        .Replace(@"\ö", "ö")
                        .Replace(@"\u022a", "Ö")
                        .Replace(@"\Ö", "Ö")
                        .Replace(@"\u011e", "Ğ")
                        .Replace(@"\Ğ", "Ğ")
                        .Replace(@"\u011f", "ğ")
                        .Replace(@"\ğ", "ğ");

            value = value.Replace(@"\ç", "ç");

            return value;
        }

        public static string ToHtmlDecode(this string text)
        {
            return WebUtility.HtmlDecode(text);
        }

        public static string ToTrim(this string value, int length)
        {
            if (string.IsNullOrEmpty(value))
                return "";

            if (value.Length <= length)
                return value;

            return value.Substring(0, length);
        }
    }
}
