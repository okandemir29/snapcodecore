﻿using System;

namespace Snapcode.Infrastructure.Extensions
{
    public static class DateExtensions
    {
        public static string ToTimeAgo(this DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} year ago", years);
            }
            if (span.Days >= 7)
                return String.Format("{0} week ago", (int)(span.Days / 7));
            if (span.Days < 7 && span.Days > 0)
                return String.Format("{0} day ago", span.Days);
            if (span.Hours > 0)
                return String.Format("{0} hour ago", span.Hours);
            if (span.Minutes > 0)
                return String.Format("{0} minutes ago", span.Minutes);
            if (span.Seconds > 5)
                return String.Format("{0} second ag", span.Seconds);
            if (span.Seconds <= 5)
                return "0s";
            return string.Empty;
        }
    }
}
