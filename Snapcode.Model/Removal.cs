﻿using System;

namespace Snapcode.Model
{
    public class Removal : Core.ModelBase
    {
        public string Username { get; set; }
        public string Mail { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string IpAddress { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsRead { get; set; }
        public bool IsDmca { get; set; }
    }
}
