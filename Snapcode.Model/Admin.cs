﻿namespace Snapcode.Model
{
    public class Admin : Core.ModelBase
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
