﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snapcode.Model.Dto
{
    public class ProfileForListSimpleDto
    {
        public int ProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string BoorCode { get; set; }
        public string OverlayPath { get; set; }
        public string Interests { get; set; }
        public string About { get; set; }
        public int CategoryId { get; set; }
    }
}
