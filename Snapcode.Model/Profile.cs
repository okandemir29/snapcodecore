﻿using System;

namespace Snapcode.Model
{
    public class Profile : Core.ModelBase
    {
        public string About { get; set; }
        public string BoorCode { get; set; }
        public int CategoryId { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OverlayPath { get; set; }
        public string Pinterest { get; set; }
        public string Username { get; set; }
        public int TotalLike { get; set; }
        public string Youtube { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Interests { get; set; }
        public bool IsNsfw { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
    }
}
