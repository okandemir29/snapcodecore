﻿using System;

namespace Snapcode.Model
{
    public class Setting : Core.ModelBase
    {
        public string Sitename { get; set; }
        public string Siteurl { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Youtube { get; set; }
        public string Instagram { get; set; }
        public string SiteHomeTitle { get; set; }
        public string SiteHomeDesription { get; set; }
        public string HeaderCode { get; set; }
        public string FooterCode { get; set; }
    }
}
