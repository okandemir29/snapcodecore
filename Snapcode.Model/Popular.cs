﻿namespace Snapcode.Model
{
    public class Popular : Core.ModelBase
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Instagram { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string Slug { get; set; }
        public bool IsBanned { get; set; }
        public int ListId { get; set; }
    }
}
