﻿using System;

namespace Snapcode.Model
{
    public class AdsAccount : Core.ModelBase
    {
        public string Email { get; set; }
        public string Ads336x280 { get; set; }
        public string Ads300x600 { get; set; }
        public string CaPubId { get; set; }
        public string Ads300Id { get; set; }
        public string Ads600Id { get; set; }
        public bool HasAds { get; set; }
        public bool HasAdsTricks { get; set; }
    }
}
