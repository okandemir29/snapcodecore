﻿namespace Snapcode.Model
{
    public class List : Core.ModelBase
    {
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
