﻿using System;

namespace Snapcode.Model
{
    public class Review : Core.ModelBase
    {
        public int UserId { get; set; }
        public string Fullname { get; set; }
        public int Star { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsPopular { get; set; }
        public bool IsActive { get; set; }
        public bool IsRead { get; set; }
    }
}
