﻿namespace Snapcode.Model
{
    public class Hashtag : Core.ModelBase
    {
        public string Name { get; set; }
        public bool isBan { get; set; }
        public bool IsDmca { get; set; }
    }
}
