﻿using System;

namespace Snapcode.Model.Core
{
    public class IgnoredAttribute : System.Attribute
    {
        public string SomeProperty { get; set; }
    }
}
